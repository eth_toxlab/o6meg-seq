# O6-meG-seq

This GitLab project supports data analysis for the paper 'Single-nucleotide-resolution genomic maps of O6-methylguanine from the glioblastoma drug temozolomide'
published on 20th January 2025 in Nucleic Acids Research (https://academic.oup.com/nar/article/53/2/gkae1320/7962014).


## Abstract
Temozolomide kills cancer cells by forming O6-methylguanine (O6-MeG), which leads to cell cycle arrest and apoptosis. However, O6-MeG repair by O6-methylguanine-DNA methyltransferase (MGMT) contributes to drug resistance. Characterizing genomic profiles of O6-MeG could elucidate how O6-MeG accumulation is influenced by repair, but there are no methods to map genomic locations of O6-MeG. Here, we developed an immunoprecipitation- and polymerase-stalling-based method, termed O6-MeG-seq, to locate O6-MeG across the whole genome at single-nucleotide resolution. We analyzed O6-MeG formation and repair across sequence contexts and functional genomic regions in relation to MGMT expression in a glioblastoma-derived cell line. O6-MeG signatures were highly similar to mutational signatures from patients previously treated with temozolomide. Furthermore, MGMT did not preferentially repair O6-MeG with respect to sequence context, chromatin state or gene expression level, however, may protect oncogenes from mutations. Finally, we found an MGMT-independent strand bias in O6-MeG accumulation in highly expressed genes. These data provide high resolution insight on how O6-MeG formation and repair are impacted by genome structure and nucleotide sequence. Further, O6-MeG-seq is expected to enable future studies of DNA modification signatures as diagnostic markers for addressing drug resistance and preventing secondary cancers.


## Data Availability
O6-meG-seq raw sequencing files and processed files can be found on NCBI Gene Expression Omnibus (GEO) (accession number GSE279423). All other data and support files can be found on Zenodo (DOI 10.5281/zenodo.10518965).

## License
MIT License, Copyright (c) 2024 Jasmina Kubitschek and Dr. Vakil Takhaveev

## Folder structure
The 'preprocessing' folder contains scripts used to process raw data to bed-files including a logbook for data processing.
The 'plotting_figures' folder contains all necessary scripts and notebooks used for the figures in the manuscript.
