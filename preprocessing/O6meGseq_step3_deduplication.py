'''
Jasmina Kubitschek
02.07.2024
after:
Dr. Vakil Takhaveev
07.06.2022 & 2024

This script implements the following:
1) Adding the read group tag necessary for deduplication,
2) Indexing necessary for deduplication,
3) Generating stats of the alignment file,
4) Deduplicating via umi-tools: method "unique",
5) Generating stats of the deduplicated file

Using the ETH Euler Batch System.
'''

import pandas as pd
import argparse
import os
import time

###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-p", "--path", help = "Path", required = True)
argument = parser.parse_args()

working_path = argument.path

bash_script_templ = '''#!/bin/bash
#SBATCH -J "Step3__sample_"
#SBATCH --time=_TIME_
#SBATCH -n 1
#SBATCH --mem-per-cpu=_MEMORY_
#SBATCH -e _samplefolder_/_sample_.step3.err
#SBATCH -o _samplefolder_/_sample_.step3.out

module load stack/2024-06
module load jdk/8u141-b15
module load python_cuda
module load samtools/1.17

echo '### Adding the read goup tag necessary for deduplication'
samtools addreplacerg -r ID:_sample_ -m overwrite_all -o _tagged_ -@ 1 _sorted_output_

echo '### Indexing before deduplication'
samtools index _tagged_

echo '### Stats after sorting and adding RG'
samtools stats -@ 1 _tagged_ > _tagging_stats_

echo '### Deduplication'
$HOME/programs/gatk-4.2.0.0/gatk MarkDuplicatesSpark -I _tagged_ -O _deduplicated_ -M  _dedup_metrics_ --remove-all-duplicates true

echo '### Stats after deduplication'
samtools stats -@ 1 _deduplicated_ > _dedup_stats_

'''

# A function to replace the names in the bash script template
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

# Adding folders
step2_folder = os.path.join(working_path, 'Step2_alignment')
step3_folder = os.path.join(working_path, 'Step3_deduplication')
os.mkdir(step3_folder)

# Determining the maximal size among the input file
max_MB_size = 0
for sample in os.listdir(step2_folder):
    sample_input_folder = os.path.join(step2_folder, sample)
    sorted_output = os.path.join(sample_input_folder, sample + '_sorted.bam')
    MB_size = os.path.getsize(sorted_output)/(10**6)
    if MB_size > max_MB_size:
        max_MB_size = MB_size

for sample in os.listdir(step2_folder):
    sample_input_folder = os.path.join(step2_folder, sample)
    sample_output_folder = os.path.join(step3_folder, sample)
    os.mkdir(sample_output_folder)
    
    sorted_output = os.path.join(sample_input_folder, sample + '_sorted.bam')
    tagged = os.path.join(sample_output_folder, sample + '_tagged.bam')
    tagging_stats = os.path.join(sample_output_folder, sample + '_tagging_stats.txt')
    deduplicated = os.path.join(sample_output_folder, sample + '_dedup.bam')
    dedup_metrics = os.path.join(sample_output_folder, sample + '_dedup_metrics.txt')
    dedup_stats = os.path.join(sample_output_folder, sample + '_dedup_stats.txt')

    MB_size = os.path.getsize(sorted_output)/(10**6)
    
    ### Setting the computation time
    MB_size = os.path.getsize(sorted_output)/(10**6)
    comp_time = 3.5*MB_size/max_MB_size
    i, d = divmod(comp_time, 1)
    i, d = int(i), int(60*d)
    if i < 4:
        i = 3
        d = 58
    comp_time = str(i).zfill(2) + ':' + str(d).zfill(2) + ':00'


    ### Setting the computation time
    if MB_size < 250:
        MB_size = 250
    comp_mem = str(int(round(10*MB_size)))

    print(sample, comp_time, comp_mem)

    sh_name = os.path.join(sample_output_folder, sample + "_O6meGseq_deduplication_step3.sh")
    with open(sh_name, 'w') as job_script:
        job_script.write(
            replace_all(bash_script_templ, {"_sample_" : sample,
                                            "_samplefolder_" : sample_output_folder,
                                            "_sorted_output_" : sorted_output,
                                            "_tagged_" : tagged,
                                            "_deduplicated_" : deduplicated,
                                            "_tagging_stats_" : tagging_stats,
                                            "_dedup_stats_" : dedup_stats,
                                            "_dedup_metrics_" : dedup_metrics,
                                            "_TIME_" : comp_time,
                                            "_MEMORY_" : comp_mem})
        )

    os.system("sbatch < " + sh_name)
    time.sleep(2)

