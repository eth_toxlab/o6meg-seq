'''
Jasmina Kubitschek
01.07.2024
after:
Dr. Vakil Takhaveev
07.06.2022 & 2024

This script is for parallel implementation of the following:
1) Aligning the reads to the reference genome,
2) Sorting the bam file

Using the ETH Euler Batch System.
'''

import pandas as pd
import argparse
import os
import time

###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-p", "--path", help = "The path to the folder in which the analysis is performed", required = True)

argument = parser.parse_args()
working_path = argument.path

bash_script_templ = '''#!/bin/bash
#SBATCH -J "Step2__sample_"
#SBATCH --time=_TIME_
#SBATCH -n 6
#SBATCH --mem-per-cpu=_MEMORY_
#SBATCH -e _samplefolder_/_sample_.step2.err
#SBATCH -o _samplefolder_/_sample_.step2.out

module load stack/2024-06
module load bowtie2/2.5.1-u2j3omo
module load samtools/1.17

echo '### Aligning the reads to the references'
bowtie2 -x /cluster/home/jabueche/genomes/GRCh38_NCBI_Bowtie2_index/GRCh38_noalt_as/GRCh38_noalt_as -1 _inputR1_ -2 _inputR2_  -S _bowtie_output_ --no-mixed --no-discordant -p 6

echo '### Clean up read pairing information and flags'
samtools fixmate -O bam -@ 6 _bowtie_output_ _fixmate_output_

echo '### Sorting the bam files'
samtools sort -O bam -@ 6 -o _sorted_output_ -T _tmpfolder_samtools_sort_ _fixmate_output_

'''

# A function to replace the names in the bash script template
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

# Adding folders
step1_folder = os.path.join(working_path, 'Step1_adapt_remov')
step2_folder = os.path.join(working_path, 'Step2_alignment')
os.mkdir(step2_folder)


# Determining the maximal size among the input file
max_MB_size = 0
for sample in os.listdir(step1_folder):
    sample_input_folder = os.path.join(step1_folder, sample)
    input_R1 = os.path.join(sample_input_folder, 'Output_step1_R1_' + sample +'_R1_001.fastq.gz')
    MB_size = os.path.getsize(input_R1)/(10**6)
    if MB_size > max_MB_size:
        max_MB_size = MB_size


for sample in os.listdir(step1_folder):
    sample_input_folder = os.path.join(step1_folder, sample)
    sample_output_folder = os.path.join(step2_folder, sample)
    os.mkdir(sample_output_folder)
    tmpfolder_samtools_sort = os.path.join(sample_output_folder, 'tmp_samtools_sort')
    os.mkdir(tmpfolder_samtools_sort)

    input_R1 = os.path.join(sample_input_folder, 'Output_step1_R1_' + sample +'_R1_001.fastq.gz')
    input_R2 = os.path.join(sample_input_folder, 'Output_step1_R2_' + sample +'_R2_001.fastq.gz')
    bowtie_output = os.path.join(sample_output_folder, sample + '_aligned.sam')
    fixmate_output = os.path.join(sample_output_folder, sample + '_fixmate.bam')
    sorted_output = os.path.join(sample_output_folder, sample + '_sorted.bam')

    MB_size = os.path.getsize(input_R1)/(10**6)

    ### Setting the computation time
    MB_size = os.path.getsize(input_R1)/(10**6)
    comp_time = 2.0*MB_size/max_MB_size
    i, d = divmod(comp_time, 1)
    i, d = int(i), int(60*d)
    if i < 4:
        i = 3
        d = 58
    comp_time = str(i).zfill(2) + ':' + str(d).zfill(2) + ':00'

    ### Setting the computation time
    if MB_size < 1000:
        MB_size = 1000
    comp_mem = str(int(round(1*MB_size)))

    print(sample, comp_time, comp_mem)

    sh_name = os.path.join(sample_output_folder, sample + "_O6meGseq_alignment_step2.sh")
    with open(sh_name, 'w') as job_script:
        job_script.write(
            replace_all(bash_script_templ, {"_sample_" : sample,
                                            "_samplefolder_" : sample_output_folder,
                                            "_tmpfolder_samtools_sort_" : tmpfolder_samtools_sort,
                                            "_inputR1_" : input_R1,
                                            "_inputR2_" : input_R2,
                                            "_bowtie_output_" : bowtie_output,
                                            "_fixmate_output_" : fixmate_output,
                                            "_sorted_output_" : sorted_output,
                                            "_TIME_" : comp_time,
                                            "_MEMORY_" : comp_mem})
        )

    os.system("sbatch < " + sh_name)
    time.sleep(2)

