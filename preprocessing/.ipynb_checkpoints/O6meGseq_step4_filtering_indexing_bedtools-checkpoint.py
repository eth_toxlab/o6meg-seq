'''
Jasmina Kubitschek
01.07.2024
after:
Dr. Vakil Takhaveev
11.08.2022 & 2024

This script implements the following:
1) Filtering out unmapped reads,
2) Generating stats after filtering out unmapped reads,
3) Indexing,
4) Extracting the sequence -10 to +10 regarding the damage site

Using the ETH Euler Batch System.
'''

import pandas as pd
import argparse
import os
import time

###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-p", "--path", help = "Path", required = True)

argument = parser.parse_args()
working_path = argument.path


bash_script_templ = '''#!/bin/bash
#SBATCH -J "Step4__sample_"
#SBATCH --time=_TIME_
#SBATCH -n 1
#SBATCH --mem-per-cpu=_MEMORY_
#SBATCH -e _samplefolder_/_sample_.step4.err
#SBATCH -o _samplefolder_/_sample_.step4.out

module load stack/2024-06
module load python_cuda
module load samtools/1.17
module load bedtools2/2.31.0

echo '### Filtering out unmapped reads'
samtools view -b -F 4 _input_ > _filtered_

echo '### Generating stats after filtering unmapped reads'
samtools stats _filtered_ > _filtstats_

echo '### Indexing'
samtools index _filtered_

echo '### Bam2Bed'
bedtools bamtobed -i _filtered_ > _filtbed_

echo '###Extracting coordinates of regarding the damage position -1 re. the 5pr-end'
awk -v OFS='\t' '{if ($6=="+") {$3=$2;$2=$2-1} else {$2=$3;$3=$3+1}} {print}' _filtbed_ > _coord1nt_ #for -1 position only
awk -v OFS='\t' '{if ($6=="+") {$3=$2+1;$2=$2-2} else {$2=$3-1;$3=$3+2}} {print}' _filtbed_ > _coord3nt_ #trinucleotide context
awk -v OFS='\t' '{if ($6=="+") {$3=$2+10;$2=$2-11} else {$2=$3-10;$3=$3+11}} {print}' _filtbed_ > _coord21nt_ #21nt

echo '### Number of reads after filtering unmapped reads and calculating read start and end'
cat _coord1nt_ | wc -l
cat _coord3nt_ | wc -l
cat _coord21nt_ | wc -l

echo '###Number of cases when the -_W_ position is negative'
awk -v OFS='\t' '$2<0 {print}' _coord1nt_ | wc -l
awk -v OFS='\t' '$2<0 {print}' _coord3nt_ | wc -l
awk -v OFS='\t' '$2<0 {print}' _coord21nt_ | wc -l

echo '###Removing the cases when the -_W_ position is negative'
awk -v OFS='\t' '$2>=0 {print}' _coord1nt_ > _nozero1nt_
awk -v OFS='\t' '$2>=0 {print}' _coord3nt_ > _nozero3nt_
awk -v OFS='\t' '$2>=0 {print}' _coord21nt_ > _nozero21nt_

echo '### Number of reads after removing reads >=0'
cat _nozero1nt_ | wc -l
cat _nozero3nt_ | wc -l
cat _nozero21nt_ | wc -l

echo '###Keeping only R1'
awk -v OFS='\t' '$4 ~ /\/1$/ {print}' _nozero1nt_ > _1ntR1_
awk -v OFS='\t' '$4 ~ /\/1$/ {print}' _nozero3nt_ > _3ntR1_
awk -v OFS='\t' '$4 ~ /\/1$/ {print}' _nozero21nt_ > _21ntR1_

echo '#### Number of R1 reads'
cat _1ntR1_ | wc -l
cat _3ntR1_ | wc -l
cat _21ntR1_ | wc -l

echo '###Number of cases with MAPQ below 20'
awk -v OFS='\t' '$5<20 {print}' _1ntR1_ | wc -l
awk -v OFS='\t' '$5<20 {print}' _3ntR1_ | wc -l
awk -v OFS='\t' '$5<20 {print}' _21ntR1_ | wc -l

echo '###Removing the cases with MAPQ below 20'
awk -v OFS='\t' '$5>=20 {print}' _1ntR1_ > _1ntR1MAPQ_
awk -v OFS='\t' '$5>=20 {print}' _3ntR1_ > _3ntR1MAPQ_
awk -v OFS='\t' '$5>=20 {print}' _21ntR1_ > _21ntR1MAPQ_

echo '#### Number of R1 reads with MAPQ >=20'
cat _1ntR1MAPQ_ | wc -l
cat _3ntR1MAPQ_ | wc -l
cat _21ntR1MAPQ_ | wc -l

echo '###Extracting the sequence -_W_ to +_W_+1 regarding the position downstream the damage for R1 reads with and without MAPQ filter'
bedtools getfasta -fi /cluster/home/jabueche/genomes/GRCh38_NCBI_Bowtie2_index/GRCh38_noalt_as.fasta -bed _1ntR1_ -bedOut -s > _seq1nt_
bedtools getfasta -fi /cluster/home/jabueche/genomes/GRCh38_NCBI_Bowtie2_index/GRCh38_noalt_as.fasta -bed _3ntR1_ -bedOut -s > _seq3nt_
bedtools getfasta -fi /cluster/home/jabueche/genomes/GRCh38_NCBI_Bowtie2_index/GRCh38_noalt_as.fasta -bed _21ntR1_ -bedOut -s > _seq21nt_
bedtools getfasta -fi /cluster/home/jabueche/genomes/GRCh38_NCBI_Bowtie2_index/GRCh38_noalt_as.fasta -bed _1ntR1MAPQ_ -bedOut -s > _seq1ntMAPQ_
bedtools getfasta -fi /cluster/home/jabueche/genomes/GRCh38_NCBI_Bowtie2_index/GRCh38_noalt_as.fasta -bed _3ntR1MAPQ_ -bedOut -s > _seq3ntMAPQ_
bedtools getfasta -fi /cluster/home/jabueche/genomes/GRCh38_NCBI_Bowtie2_index/GRCh38_noalt_as.fasta -bed _21ntR1MAPQ_ -bedOut -s > _seq21ntMAPQ_


'''

# A function to replace the names in the bash script template
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

# Adding folders
step3_folder = os.path.join(working_path, 'Step3_deduplication')
step4_folder = os.path.join(working_path, 'Step4_bedtools')
os.mkdir(step4_folder)

# Determining the maximal size among the input file
max_MB_size = 0
for sample in os.listdir(step3_folder):
    sample_input_folder = os.path.join(step3_folder, sample)
    input_file = os.path.join(sample_input_folder, sample + '_dedup.bam')
    MB_size = os.path.getsize(input_file)/(10**6)
    if MB_size > max_MB_size:
        max_MB_size = MB_size

for sample in os.listdir(step3_folder):
    sample_input_folder = os.path.join(step3_folder, sample)
    sample_output_folder = os.path.join(step4_folder, sample)
    os.mkdir(sample_output_folder)
    
    input_file = os.path.join(sample_input_folder, sample + '_dedup.bam')
    
    filtered = os.path.join(sample_output_folder, sample + '_filtered.bam')
    filt_stats = os.path.join(sample_output_folder, sample + '_filtered_stats.bam')
    filt_bed = os.path.join(sample_output_folder, sample + '_filtered.bed')
    
    coord_1nt = os.path.join(sample_output_folder, sample + '_coord_1nt.bed')
    coord_3nt = os.path.join(sample_output_folder, sample + '_coord_3nt.bed')
    coord_21nt = os.path.join(sample_output_folder, sample + '_coord_21nt.bed')
    
    nozero_1nt = os.path.join(sample_output_folder, sample + '_coordnozero_1nt.bed')
    nozero_3nt = os.path.join(sample_output_folder, sample + '_coordnozero_3nt.bed')
    nozero_21nt = os.path.join(sample_output_folder, sample + '_coordnozero_21nt.bed')

    R1_only_1nt = os.path.join(sample_output_folder, sample + '_R1_1nt.bed')
    R1_only_3nt = os.path.join(sample_output_folder, sample + '_R1_3nt.bed')
    R1_only_21nt = os.path.join(sample_output_folder, sample + '_R1_21nt.bed')

    MAPQ20_1nt = os.path.join(sample_output_folder, sample + '_R1_MAPQ20_1nt.bed')
    MAPQ20_3nt = os.path.join(sample_output_folder, sample + '_R1_MAPQ20_3nt.bed')
    MAPQ20_21nt = os.path.join(sample_output_folder, sample + '_R1_MAPQ20_21nt.bed')

    seq_1nt = os.path.join(sample_output_folder, sample + '_seq_1nt.bed')
    seq_3nt = os.path.join(sample_output_folder, sample + '_seq_3nt.bed')
    seq_21nt = os.path.join(sample_output_folder, sample + '_seq_21nt.bed')

    seq_1nt_MAPQ = os.path.join(sample_output_folder, sample + '_MAPQ20_seq_1nt.bed')
    seq_3nt_MAPQ = os.path.join(sample_output_folder, sample + '_MAPQ20_seq_3nt.bed')
    seq_21nt_MAPQ = os.path.join(sample_output_folder, sample + '_MAPQ20_seq_21nt.bed')    

    MB_size = os.path.getsize(input_file)/(10**6)

    
    ### Setting the computation time
    comp_time = 3.9*MB_size/max_MB_size
    i, d = divmod(comp_time, 1)
    i, d = int(i), int(60*d)
    if i == 0 and d < 15:
        d = 15
    comp_time = str(i).zfill(2) + ':' + str(d).zfill(2) + ':00'
    

    #comp_time = '03:50:00'

    ### Setting the computation memory
    if MB_size < 150:
        MB_size = 150
    comp_mem = str(int(round(5*MB_size)))

    print(sample, comp_time, comp_mem)

    sh_name = os.path.join(sample_output_folder, sample + "_O6meGseq_bedtools_step4.sh")
    with open(sh_name, 'w') as job_script:
        job_script.write(
            replace_all(bash_script_templ, {"_sample_" : sample, 
                                            "_samplefolder_" : sample_output_folder,
                                            "_input_" : input_file, 
                                            "_filtered_" : filtered,
                                            "_filtbed_" : filt_bed, 
                                            
                                            "_coord1nt_" : coord_1nt,
                                            "_nozero1nt_" : nozero_1nt, 
                                            "_1ntR1_" : R1_only_1nt,
                                            "_1ntR1MAPQ_" : MAPQ20_1nt,
                                            "_seq1nt_" : seq_1nt,
                                            "_seq1ntMAPQ_" : seq_1nt_MAPQ,
                                            
                                            "_coord3nt_" : coord_3nt,
                                            "_nozero3nt_" : nozero_3nt,
                                            "_3ntR1_" : R1_only_3nt,
                                            "_3ntR1MAPQ_" : MAPQ20_3nt,
                                            "_seq3nt_" : seq_3nt,
                                            "_seq3ntMAPQ_" : seq_3nt_MAPQ,
                                            
                                            "_coord21nt_" : coord_21nt,
                                            "_nozero21nt_" : nozero_21nt, 
                                            "_21ntR1_" : R1_only_21nt,
                                            "_21ntR1MAPQ_" : MAPQ20_21nt,
                                            "_seq21nt_" : seq_21nt,
                                            "_seq21ntMAPQ_" : seq_21nt_MAPQ,
                                            
                                            "_filtstats_" : filt_stats,
                                            "_TIME_" : comp_time, 
                                            "_MEMORY_" : comp_mem})
        )

    os.system("sbatch < " + sh_name)
    time.sleep(2)

