'''
Jasmina Kubitschek
07.08.2024
after:
Dr. Vakil Takhaveev
07.06.2022 & 2024

This script is for parallel implementation of the following:
1) Removing low-quality reads
2) Remooving reads with AD1B
3) Crop 12bp head of R2 due to bad base content

Using the ETH Euler Batch System.
'''

# import pandas as pd
import argparse
import os, sys, stat
import shutil
import time

###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-p", "--path", help = "The path to the folder in which the analysis is performed", required = True)
parser.add_argument("-i", "--input", help = "The path to the folder containing the input fastq.gz files", required = True)

argument = parser.parse_args()
working_path = argument.path
inputpath = argument.input

bash_script_templ = '''#!/bin/bash
#SBATCH -J "Step1__sample_"
#SBATCH --time=_TIME_
#SBATCH -n 1
#SBATCH --mem-per-cpu=_MEMORY_
#SBATCH -e _samplefolder_/_sample_.step1.err
#SBATCH -o _samplefolder_/_sample_.step1.out

module load stack/2024-06
module load gcc/12.2.0
module load jdk/8u141-b15
module load py-dnaio/0.10.0-l2umcaf
module load py-xopen/1.6.0-o55adyx
module load py-cutadapt/4.4-jfcyzb5
module load fastqc/0.12.1
module load trimmomatic/0.39
module load bbmap/39.01

echo '### Removing Illumina adapters and low quality reads'
trimmomatic PE -trimlog _samplefolder_/_sample__trim_LOG.log _input_file_R1_ _input_file_R2_ _trimmed_R1_paired_ _trimmed_R1_unpaired_ _trimmed_R2_paired_ _trimmed_R2_unpaired_ ILLUMINACLIP:/cluster/home/jabueche/programs/Trimmomatic-0.39/adapters/TruSeq3-PE-2.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:151

echo '### Discarding AD1B reads'
cutadapt -b "GACTGGTTCCAATTGAAAGTGCTCTTCCGATCT;e=0.1;min_overlap=15" -B "AGATCGGAAGAGCACTTTCAATTGGAACCAGTC;e=0.1;min_overlap=15" --pair-filter=any --discard-trimmed -o _AD1Bout_file_R1_ -p _AD1Bout_file_R2_ _trimmed_R1_paired_ _trimmed_R2_paired_

echo '### Crop 12bp of R2 5-prime end due to bad base content'
trimmomatic SE _AD1Bout_file_R2_ _R2_trimmed12_ HEADCROP:12 MINLEN:139

echo '### Check pairing of R1 and R2 files'
repair.sh in1=_AD1Bout_file_R1_ in2=_R2_trimmed12_ out1=_output_R1_paired_ out2=_output_R2_paired_ outs=_singletons_

'''

# A function to replace the names in the bash script template
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

            
step1_folder = os.path.join(working_path, 'Step1_adapt_remov')
os.mkdir(step1_folder)

inputpath_R1 = os.path.join(inputpath, 'R1')
inputpath_R2 = os.path.join(inputpath, 'R2')

# Determining the maximal size among the input file
max_MB_size = 0
for file in os.listdir(inputpath_R1):
        input_filepath = os.path.join(inputpath_R1, file)
        MB_size = os.path.getsize(input_filepath)/(10**6)
        if MB_size > max_MB_size:
            max_MB_size = MB_size

for file in os.listdir(inputpath_R1):
    parts = file.split('_')
    sample = '_'.join(parts[:-2])
    sample_R1 = sample+'_R1_001.fastq.gz'
    sample_R2 = sample+'_R2_001.fastq.gz'
    sample_folder = os.path.join(step1_folder, sample)
    os.mkdir(sample_folder)


for file in os.listdir(inputpath_R1):
    parts = file.split('_')
    sample = '_'.join(parts[:-2])
    sample_R1 = sample+'_R1_001.fastq.gz'
    sample_R2 = sample+'_R2_001.fastq.gz'
    sample_folder = os.path.join(step1_folder, sample)
    
    input_file_R1 = os.path.join(inputpath_R1, sample_R1)
    trimmed_R1_paired = os.path.join(sample_folder, 'trimmed_paired_' + sample_R1)
    trimmed_R1_unpaired = os.path.join(sample_folder, 'trimmed_unpaired_' + sample_R1)
    AD1Bout_file_R1 = os.path.join(sample_folder, 'AD1Bout_' + sample_R1)
    
    input_file_R2 = os.path.join(inputpath_R2, sample_R2)
    trimmed_R2_paired = os.path.join(sample_folder, 'trimmed_paired_' + sample_R2)
    trimmed_R2_unpaired = os.path.join(sample_folder, 'trimmed_unpaired_' + sample_R2)
    AD1Bout_file_R2 = os.path.join(sample_folder, 'AD1Bout_' + sample_R2)


    AD1Bout_file_R2_trimmed12 = os.path.join(sample_folder, 'R2_trim12_' + sample_R2)
    output_R1_paired = os.path.join(sample_folder, 'Output_step1_R1_' + sample_R1)
    output_R2_paired = os.path.join(sample_folder, 'Output_step1_R2_' + sample_R2)
    singletons = os.path.join(sample_folder, 'singletons_repairing_' + sample)
    
        
    ### Setting the computation time
    
    MB_size = os.path.getsize(input_file_R1)/(10**6)
    comp_time = 5.5*MB_size/max_MB_size
    i, d = divmod(comp_time, 1)
    i, d = int(i), int(60*d)
    if i < 4:
        i = 3
        d = 58
    comp_time = str(i).zfill(2) + ':' + str(d).zfill(2) + ':00'

    
    ### Setting the computation memory
    MB_size = os.path.getsize(input_file_R1)/(10**6)
    comp_mem = str(int(round(3*MB_size)))
    if float(comp_mem) < 1000:
        comp_mem = '1000'
    print(sample, comp_time, comp_mem)
    
    sh_name = os.path.join(sample_folder, sample + "_O6meGseq_step1_adapter_removal.sh")
    with open(sh_name, 'w') as job_script:
            job_script.write(
                replace_all(bash_script_templ, {"_sample_" : sample,
                                                "_samplefolder_" : sample_folder,
                                                "_input_file_R1_" : input_file_R1,
                                                "_input_file_R2_" : input_file_R2,
                                                "_trimmed_R1_paired_" : trimmed_R1_paired,
                                                "_trimmed_R1_unpaired_" : trimmed_R1_unpaired,
                                                "_trimmed_R2_paired_" : trimmed_R2_paired,
                                                "_trimmed_R2_unpaired_" : trimmed_R2_unpaired,
                                                "_AD1Bout_file_R1_" : AD1Bout_file_R1,
                                                "_AD1Bout_file_R2_" : AD1Bout_file_R2,
                                                "_R2_trimmed12_" : AD1Bout_file_R2_trimmed12,
                                                "_output_R1_paired_" : output_R1_paired,
                                                "_output_R2_paired_" : output_R2_paired,
                                                "_singletons_" : singletons,
                                                "_TIME_" : comp_time,
                                                "_MEMORY_" : comp_mem})
            )

    os.system("sbatch < " + sh_name)
    time.sleep(2)

 