#importing necessary modules
import os
import numpy as np
import pandas as pd
import scipy
import sys
from itertools import product

import argparse

###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-o", "--path", help = "The path to output folder", required = True)
parser.add_argument("-s", "--sample", help = "The sample (file) name", required = True)
argument = parser.parse_args()

outpath = argument.path
sample = argument.sample


chromosomes = ['chr' + str(i) for i in np.arange(1, 23, 1)] + ["chrX"]


p = '/nfs/nas12.ethz.ch/fs1201/green_groups_let_public/Euler/O6meG_revision/gene_annotation/'
PATH = '/nfs/nas12.ethz.ch/fs1201/green_groups_let_public/Euler/O6meG_revision/bedgraph_files/'

prefix = ""
suffix = "_"

strands = {"sense" : [("plus", "plus"), ("minus", "minus")],
           "antisense" : [("plus", "minus"), ("minus", "plus")]}
#(gene's strand, damage signal's strand)


WINDOW = 50000

DATA = pd.DataFrame({})

for s in strands:
            for ss in strands[s]:
                df1 = pd.read_csv(p + "knownGenes_canonTr_" + ss[0] + "_strand.bed", 
                                  sep = "\t", header = None, 
                                  names = ["Chr", "Gene_start", "Gene_end", "Gene", "Transcript", "Strand"])
                
                # +- WINDOW bp regarding TES
                # both boundaries are inclusive
                # length: 2*WINDOW + 1
                if ss[0] == "plus":
                    df1.loc[:, "Upstr_boundary_TES"] = df1["Gene_end"] - WINDOW - 1 #I subtracted 1 because Gene_end is non-inclusive, TES is at (Gene_end - 1)
                    df1.loc[:, "Downstr_boundary_TES"] = df1["Gene_end"] + WINDOW - 1 #I subtracted 1 because Gene_end is non-inclusive, TES is at (Gene_end - 1)
                if ss[0] == "minus":
                    df1.loc[:, "Upstr_boundary_TES"] = df1["Gene_start"] + WINDOW
                    df1.loc[:, "Downstr_boundary_TES"] = df1["Gene_start"] - WINDOW
                               
            
                df2 = os.path.join(PATH, sample + '_' + ss[1] + "_strand.bedgraph")
                df2 = pd.read_csv(df2, sep = "\t", header = None, 
                                  names = ["Chr", "Start", "End", "Value", "MAPQ"])
                
                df2 = df2.loc[:, ["Chr", "Start", "End", "Value"]].copy()

                for chrom in chromosomes:
                    df1_ch = df1[df1["Chr"] == chrom].copy().reset_index(drop = True)
                    df2_ch = df2[df2["Chr"] == chrom].copy().reset_index(drop = True)
                    
                    features = ["TES"]
                    feature_bounds = [("Upstr_boundary_TES", "Downstr_boundary_TES")]
                    
                    for index, feature in enumerate(features):
                        a = np.array(df2_ch["Start"].values)
                        
                        start = feature_bounds[index][0]
                        end = feature_bounds[index][1]
                        
                        bl, bh = None, None
                        if ss[0] == "plus":
                            bl = np.array(df1_ch[start].values)
                            bh = np.array(df1_ch[end].values)
                        if ss[0] == "minus":
                            bl = np.array(df1_ch[end].values)
                            bh = np.array(df1_ch[start].values)
                        
                        i, j = np.where((a[:, None] >= bl) & (a[:, None] <= bh))
                        tmp = pd.concat([
                                df2_ch.loc[i, :].reset_index(drop=True),
                                df1_ch.loc[j, :].reset_index(drop=True)
                            ], axis=1)
                            
                        if ss[0] == "plus": 
                            tmp.loc[:, "Position_rel_" + feature] = tmp["Start"] - (tmp["Gene_end"] - 1)
                            #I subtracted 1 because Gene_end is non-inclusive, TES is at (Gene_end - 1)
                        if ss[0] == "minus": 
                            tmp.loc[:, "Position_rel_" + feature] = -1*(tmp["Start"] - tmp["Gene_start"])
                            
                            
                        #tmp["Feature"] = feature
                        tmp["Sample"] = sample
                        tmp["Strand"] = s
                        
                        DATA = pd.concat([DATA, tmp])
          
            print(sample, s)

DATA.to_csv(outpath + sample + "_window_50000.csv")
