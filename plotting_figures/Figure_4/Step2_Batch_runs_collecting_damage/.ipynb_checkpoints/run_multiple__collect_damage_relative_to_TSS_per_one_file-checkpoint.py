'''
Jasmina Kubitschek
29.07.2024
after Dr. Vakil Takhaveev
15.11.2023
'''

import pandas as pd
import argparse
import os
import time

###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-o", "--path", help = "The path to output folder", required = True)
argument = parser.parse_args()

PATH = argument.path

bash_script_templ = '''#!/bin/bash

#SBATCH -n 1
#SBATCH --cpus-per-task=1
#SBATCH --time=00:40:00
#SBATCH --job-name=TSS__SAMPLE_
#SBATCH --mem-per-cpu=7G
#SBATCH --output='_PATH_logs/ST_END__SAMPLE_.err'
#SBATCH --error='_PATH_logs/ST_END__SAMPLE_.out'
#SBATCH --open-mode=truncate

module load stack/2024-06
module load python_cuda

python3 /cluster/home/jabueche/GitLab/o6meg-seq/plotting_figures/Figure_4/Step2_Batch_runs_collecting_damage/collect_damage_relative_to_TSS_per_one_file.py -o _PATH_ -s _SAMPLE_
'''

# A function to replace the names in the bash script template
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

lookup = pd.read_csv('/nfs/nas12.ethz.ch/fs1201/green_groups_let_public/Euler/O6meG_revision/Plotting/sample_lookup.csv',index_col=0)

for INDEX, row in lookup.iterrows():
    SAMPLE = row["sample"]
    print(SAMPLE)

    sh_name = os.path.join(PATH + 'logs/', SAMPLE + ".sh")
    with open(sh_name, 'w') as job_script:
        job_script.write(
            replace_all(bash_script_templ, {"_SAMPLE_" : SAMPLE, "_PATH_" : PATH})
        )
    
    os.system("sbatch < " + sh_name)
    time.sleep(2)

