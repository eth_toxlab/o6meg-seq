''' This script uses all files in a folder (need to be bed files with triplet sequence)
and calculates the triplet counts.
Jasmina Büchel, 11.06.2023

The genomic_triplets_file can be found in the O6-meG-seq source data called "Trinucleotide_counts_genome_GRCh38.csv".'''


import numpy as np
import pandas as pd
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import argparse
from Bio.Seq import Seq
from scipy import stats


parser = argparse.ArgumentParser(description = "Optional arguments description")
parser.add_argument("-i", "--inputpath", help = "Input path of the bed file containing 3nt_context.", required = True)
parser.add_argument("-g", "--genomic_triplets_file", help = "Path to reference genome containing counts of triplets in human genome.", required = True)
parser.add_argument("-o", "--out", help = "Path incl. the output prefix", required = True)
argument = parser.parse_args()

inputpath = argument.inputpath
genomic_triplets_file = argument.genomic_triplets_file
outputpath = argument.out

# reference genome containing counts of triplets in human genome
ref_df = pd.read_csv(genomic_triplets_file, index_col = 0)
ref_df.rename(columns={"Count": "ref_count"}, inplace=True)

# read bed-file using read_csv and tab separator "\t"
bedframe = pd.read_csv(inputpath, sep = "\t", header = None)

header = ['chrom', 'chromStart', 'chromEnd', 'name', 'score', 'strand', '3nt_context']
bedframe.columns = header[:len(bedframe.columns)]

df_from_bedframe =  bedframe.loc[:, ['3nt_context']]


# make new dataframe with triplet counts
triplet_count = df_from_bedframe["3nt_context"].value_counts().reset_index()
triplet_count.rename(columns={'3nt_context': 'index'}, inplace=True)
triplet_count.rename(columns={'count': 'triplet_count'}, inplace=True)


# Merge with reference genome and add sample name (bed-file name)
ref_and_triplet_count = pd.merge(triplet_count, ref_df, on = "index", how = "left").fillna(0)
ref_and_triplet_count.rename(columns={'index': 'triplet'}, inplace=True)
ref_and_triplet_count['replicate_name'] = inputpath.split('/')[-1]


# make new dataframe with realtive counts
relative_counts = pd.DataFrame({})

relative_counts['rel_triplet'] = 100*ref_and_triplet_count['triplet_count']/ref_and_triplet_count['triplet_count'].sum()
relative_counts['rel_ref'] = 100*ref_and_triplet_count['ref_count']/ref_and_triplet_count['ref_count'].sum()
relative_counts['ratio'] = relative_counts['rel_triplet'] / relative_counts['rel_ref']
relative_counts['triplet'] = ref_and_triplet_count['triplet']
relative_counts['replicate_name'] = ref_and_triplet_count['replicate_name']


# Put triplets in order as in mutational signature.
nucleotides = ["T", "G", "C", "A"]
triplet_order = []
for i, n2 in enumerate(nucleotides):
    if n2 in nucleotides:
        for n3 in nucleotides:
            for n1 in nucleotides:
                tri_n = n1 + n2 + n3
                triplet_order.append(tri_n)
                

triplet_order = pd.DataFrame({"index" : triplet_order})
triplet_order.rename(columns={'index': 'triplet'}, inplace=True)

pattern_counts = pd.merge(triplet_order, relative_counts, on = "triplet", how = "left").fillna(0)


# save as csv
pattern_counts.to_csv(outputpath + inputpath.split('/')[-1] + "_triplet_counts"+ ".csv")

